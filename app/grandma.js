window.grandma = function(opts) {
    var $ = document.querySelector.bind(document)

    $.extend = function(a, b) {
        for (var i in b)
            a[i] = b[i]
        return a
    }

    $.addClass = function(el, classname) {
        $.delClass(el, classname)
        el.className += ' ' + classname
    }
    $.delClass = function(el, classname) {
        el.className = el.className.replace(RegExp("[ ]?" + classname, "g"), '')
    }

    $.window = window

    // Test via a getter in the options object to see if the passive property is accessed
    $.passive = undefined
    try {
        var opts = Object.defineProperty({}, 'passive', {
            get: function() {
                $.passive = {
                    passive: true
                }
            }
        });
        window.addEventListener("testPassive", null, opts);
        window.removeEventListener("testPassive", null, opts);
    } catch (e) {}

    $.require = function(url, type, next) {
        console.log("require("+url+")")
        type = type == 'js' ? 'js' : 'css'
        var tag = document.createElement(type == 'js' ? 'script' : 'link');
        if (type == 'css')
            tag.rel = "stylesheet"
        tag[type == 'js' ? 'src' : 'href'] = url;
        tag.addEventListener('load', function() {
            next(tag);
        }, $.passive);
        tag.addEventListener('error', function() {
            throw tag
            console.log('require(' + url + ') failed')
        }, $.passive);
        document.body.appendChild(tag);
    }

    $.createSelect = function(name, opts) {
        var wrapper = $.Element({
            class: "select",
            attr: {
                required: "required"
            }
        })
        var select = $.Element({
            format: '<select>',
            id: name
        })
        wrapper.appendChild(select)
        select.appendChild($.Element({
            format: '<option>',
            value: opts.default || '',
            attr: {
                disabled: "disabled",
                selected: "selected"
            }
        }))
        opts.enum.map(function(v) {
            select.appendChild($.Element({
                format: '<option>',
                value: v,
                attr: {
                    value: v
                }
            }))
        })
        return wrapper
    }

    $.createFormElement = function(name, opts) {
        elementTag = opts.format == 'textarea' ? '<textarea>' : '<input>'
        if (opts.enum)
            return $.createSelect(name, opts)
        return $.Element({
            title: opts.title || name,
            format: elementTag,
            attr: {
                type: opts.format == 'textarea' ? undefined : opts.format ? opts.format : 'text',
                id: name,
                value: opts.value || null,
                name: opts.name || null,
                class: "input",
                required: 'required',
                placeholder: opts.default || 'your ' + name
            }
        })
    }

    $.createForm = function(opts) {
        var form = $.Element({
            class: "row form center"
        })
        for (var i in opts.properties) {
            var opt = opts.properties[i]
            form.appendChild($.Element({
                format: '<label>',
                attr: {
                    id: 'form-' + i
                },
                class: opt.value ? ' ' : 'hide',
                value: opt.title || i
            }))
            var input = $.createFormElement(i, opt)
            input.oninput = function(label, opt, e) {
                $(label).className = $(label).className.replace(/hide/, '') + (String(this.value).length || this.selectedIndex ? '' : ' hide')
            }
            .bind(input, '#form-' + i, opt)
            form.appendChild(input)
        }
        if ($.get(opts, 'options.savebutton'))
            form.appendChild($.Element({
                format: "<button>",
                class: "bright-bg",
                value: opts.options.savebutton
            }))
        return form
    }

    $.Element = function(opts) {
        if (opts.type == 'object') return $.createForm(opts)
        var format = $.get(opts, 'format') || 'div'
        var tag = format ? format.replace(/[<>]/gi, '') : 'div'
        if (window.grandma.elements[format] && !opts.custom)
            return window.grandma.elements[format](opts)
        
        var el = document.createElement(tag)
        if (opts.class)
            el.className = $.renderString(opts.class, $)
        
        opts.attr  = opts.attr || {}
        opts.style = opts.style || {}  
        for (var i in opts.attr ) opts.attr[i] = $.renderString(opts.attr[i],$)
        for (var i in opts.style ) opts.style[i] = $.renderString(opts.style[i],$)
        
        if (opts.style) $.extend(el.style, opts.style)
        if (opts.attr)
            for (var i in opts.attr)
               el.setAttribute(i, $.renderString(opts.attr[i], $))    
        if (opts.value){
            var isUrl = opts.value.match(/\$\{.*[\/].*\}$/)
            if( !isUrl )
                el.innerHTML += $.renderString(opts.value, $)
            else{
                $.request('get',opts.value.replace(/(\$\{|\})/g,''),function(el,res){
                    el.innerHTML = res
                }.bind(null,el) )
            }
        }

        opts.el = el
        return el
    }

    $.all = function(selector) {
        return [].slice.call(document.querySelectorAll(selector))
    }

    $.getDefaultOpts = function() {
        var args = decodeURIComponent(String(document.location.hash).substr(1))
        args = args[0] == '{' ? new Function("return " + decodeURIComponent(args))() : {}

        var opts = $.extend({
            json: 'https://gist.githubusercontent.com/coderofsalvation/9c801b0d43c6dc67dcfb01bf70c966c8/raw/app.json?'+(new Date().getTime()),
            css: 'https://2wa.gitlab.io/grandma.js.example/app.css',
            js: 'https://2wa.gitlab.io/grandma.js.example/app.js', 
            msg_loading: "Loading.."
        }, args)

        return opts
    }

    $.init = function(opts) {
        var done = function(){ $.pub("/init/done") }        

        opts = opts || $.getDefaultOpts()
        $('#loader > #msg').innerHTML = opts.msg_loading || 'loading..'
        $.pub("/init",opts)

        $.request('get', opts.json, function(res) {
            $.require(opts.css, 'css', function() {
                $ = $.extend($, JSON.parse(res))
                $.opts = opts
                
                $.mapasync( ($.components||[]), function(c,k,next){    
                    $.require( $.renderString(c,$) ,'js', next.bind(this))
                }, function(err){
                    if(err) console.error(err)
                    $('.wrapper').innerHTML = '' // erase leftovers
                    $.cards.map(function(c) {
                        $.addCard(c)
                    })
                    if( $.wallpaper && window.innerWidth > 999 ){
                        $('#wallpaper').style.backgroundImage = 'url('+$.wallpaper+')'
                    }

                    $('#menu select').onchange = $.pub.bind($, "/menu/change")
                    setTimeout(function() {
                        $.pub("/menu/change", {
                            target: $('#menu select')
                        })
                    }, 1000)

                    if( opts.js ) $.require( opts.js,'js', done)
                    else done()
                })              
            })
        })
    }

    $.mapasync = function mapasync(arr, cb, done) {
        if( !arr || arr.length == 0 ) done() 
        var f, funcs, i, k, v;
        funcs = [];
        i = 0;
        for (k in arr) {
          v = arr[k];
          f = function(i, v) {
            return function() {
              var e, error;
              try {
                if (funcs[i + 1] != null) return cb(v, i, funcs[i + 1]);
                else return cb(v, i, done);
              } catch (error) {
                e = error;
                return done(new Error(e));
              }
            }
          }
          funcs.push(f(i++, v))
        }
        return funcs[0]()
      }

    $.addCard = function(opts) {
        opts = $.extend(opts, {
            style: {
                display: "none"
            }
        })
        var card = $.Element(opts)
        card.className = "row card"
        console.dir(opts.items.map)
        if (opts.items){
            var add = function(i) {
                card.appendChild($.Element($.extend(i, {
                    parent: card
                })))
            }
            opts.items.map( add )
            if( $.get($,'footer.items') ) $.footer.items.map(add)
            
        }
        var parent = opts.parent || $('.wrapper')
        parent.appendChild(card)
        $('#menu select').appendChild($.Element({
            format: "<option>",
            value: opts.title,
            attr: {
                value: opts.title
            }
        }))
    }

    $.get = function(xs, x, fallback ) {
        return String(x).split('.').reduce(function(acc, x) {
            if (acc == null || acc == undefined ) return fallback;
            return new Function("x","acc","return acc" + (x[0] == '[' ? x : '.'+x) )(x, acc) || fallback
        }, xs)
    }

    $.assign = function(obj, path, value) {
        var last
        var o = obj
        path = String(path)
        var vars = path.split(".")
        var lastVar = vars[vars.length - 1]
        vars.map(function(v) {
            if (lastVar == v)
                return
            o = (new Function("o","return o." + v)(o) || {})
            last = v
        })
        new Function("o","v","o." + lastVar + " = v")(o, value)
        $.pub(path, value)
        return $
    }

    $.fullscreen = function toggleFullScreen(toggle) {
        if ((document.fullScreenElement && document.fullScreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    }

    $.set = $.assign.bind($, $)

    $.on = function(path_or_data, cb) {
        var data = typeof path_or_data == 'string' ? $.get($, path_or_data) : path_or_data
        if (!data)
            return
    }

    // tiny pubsub 
    var e = $.e = {};
    $.pub = function(console, window, name, data) {
        //window.setTimeout( function(){ console.log("$.pub('"+name+"',...)") },5)
        (e[name] = e[name] || new window.Event(name)).data = data;
        window.dispatchEvent(e[name]);
    }
    .bind($, console, window)

    $.sub = function(name, handler, context) {
        console.log("$.sub('" + name + "')")
        ehandler = function(e) {
            handler(e.data)
        }
        addEventListener(name, ehandler.bind(context), $.passive);
    }

    $.unsub = function(name, handler, context) {
        removeEventListener(name, handler.bind(context));
    }

    $.initDone = function initDone() {
        $('#loader').style.display = 'none'
        $.cards.map(function(c) {
            if (c.homepage) {
                c.el.style.display = ''
                c.el.className = c.el.className + ' slidein'
            }
        })
        $('#fullscreen').addEventListener('click', $.fullscreen)
        // slide in menu
        var body = $('body')
        var variations = ['','','','','','a','b','c','d','f','g','h']
        $.addClass(body, variations[ Math.floor(Math.random()*variations.length) ])
        $.delClass(body, 'loading')
        $.addClass(body, 'loaded')
        $.addClass($('.wrapper'), 'loaded')
    }

    $.renderString = function renderString(es6_template, data) {
        if (typeof es6_template != 'string' || !data)
            return es6_template
        if (typeof data == 'function') {
            var _data = {$:$}
            for (var i in data)
                if (typeof data[i] != 'function')
                    _data[i] = data[i]
            data = _data
        }
        var reg = /\$\{(.*?)\}/gm;
        var res;
        while (res = reg.exec(es6_template)) {
            es6_template = es6_template.replace(res[0], $.get(data, res[1]) || $.get({
                window: window,
                $:$
            }, res[1]) || "")
            reg.lastIndex = 0
            // reset so duplicate template-vars will be processed 
        }
        return es6_template
    }

    //  request(method, url, payload, callback)
    $._request = function(m, u, c, x, d) {
        with (x = new XMLHttpRequest)
            return onreadystatechange = function() {
                readyState ^ 4 || c(this)
            }
            ,
            open(m, u, c),
            send(d),
            x
    }

    $.request = function(m, u, c, x, d) {
        var key = m + ' ' + String(u).replace(/\?[0-9]+/,'')
        if (!$.get(window, 'navigator.onLine'))
            return c(window.localStorage.getItem(key))
        $._request(m, u, function(res) {
            var res = res.responseText || ''
            window.localStorage.setItem(key, res)
            c(res)
        })
    }

    $.sub('/init/done', setTimeout.bind(window, $.initDone, 1000))
    $.sub('/menu/change', function(e) {
        var title = e.target.options[e.target.selectedIndex].value
        console.log(title)
        $.cards.map(function(c) {
            $.delClass(c.el, 'slidein')
            c.el.style.display = 'none'
            if (c.title == title) {
                $.addClass(c.el, 'slidein')
                c.el.style.display = ''
                c.el.style.visibility = 'visible'
            }
        })
    })

    if (opts && opts.cards) $.init(opts)

    return $
}

window.grandma.elements = {
    "img": function(opts) {
        var el = $.Element( $.extend(opts,{
            custom:true,
            format:'div',
            class: 'img',
            style: $.extend({
                    'background-image': 'url(' + opts.src + ')',
                },
                opts.style)
        }))
        return el
    },
    "iframe": function(opts) {
        var el = $.Element( $.extend(opts,{
            custom:true,
            format:"iframe",
            attr: {
                src: opts.src,
                frameborder:'0',
                hspace:'0',
                vspace:'0',
                marginheight:'0',
                marginwidth:'0',
                allowtransparency:"true"
            },
            style:{
                background: $.get(opts,'opts.style.background','none transparent'),
                width: $.get(opts,'opts.style.width',   '100%'),
                height: $.get(opts,'opts.style.height', '20vh'),
                overflow: 'hidden' 
            }
        }))
        return el
    }
}
