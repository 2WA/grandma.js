window.grandma.elements.gmaps = function(opts){
    var el = $.Element({
        attr: {
            id:"gmaps"  
        },
        style:{
            height: !opts.fullscreen && opts.height ? opts.height : '100%',
            width: !opts.fullscreen && opts.width ? opts.width : '100%',
            left: '0',
            top: '0',
            position:'absolute'
        }
    })
    if( opts.fullscreen ){
        opts.parent.style.position = 'absolute'
        opts.parent.style.left = '0'
        opts.parent.style.right = '0'
        opts.parent.style.top = '0'
        opts.parent.style.bottom = '0'
        opts.parent.style.margin = '0'
        opts.parent.style.padding = '0'
    }
    
    $.require('https://maps.googleapis.com/maps/api/js?sensor=false&extension=.js','js',function(tag){
        var style = [
            {
            stylers: [
                { saturation: "-100" },
                { lightness: "20" }
            ]
            },{
            featureType: "poi",
            stylers: [
                { visibility: "off" }
            ]
            },{
            featureType: "transit",
            stylers: [
                { visibility: "off" }
            ]
            },{
            featureType: "road",
            stylers: [
                { lightness: "50" },
                { visibility: "on" }
            ]
            },{
            featureType: "landscape",
            stylers: [
                { lightness: "50" }
            ]
            }
        ]

        var options = {
            zoom: 7,
            center:  new google.maps.LatLng(45.50867, -73.553992),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        };

        map = new google.maps.Map(el, options);
        map.setOptions({
            styles: style
        });
        console.log("maps loaded")
    })

    return el
}