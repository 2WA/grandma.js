// easy debugging on touch devices:
// put 3 fingers on the screen to activate it  

var debug = function(){


  if( ( /(android|iphone|ipad)/gi ).test( navigator.appVersion ) ) {

    var actived = false

    var log = console && console.log ? console.log.bind(console) : function(){}
    console = console || {}
    console._log = []
    
    console.log = function() {
      var arr = []
      var str = ""
      for ( var i = 0; i < arguments.length; i++ ) {
        str += String(arguments[ i ])
        arr.push( arguments[ i ] )
      }
      console._log.push( arr.join( ", ") )
      log( String(str) )
    }

    console.trace = function(){
      var stack
      try {
        throw new Error()
      } catch( ex ) {
        stack = ex.stack
      }
      console.log( "console.trace()\n" + stack.split( "\n" ).slice( 2 ).join( "  \n" ) )
    }

    console.dir = function( obj ) {
      try{
        console.log(JSON.stringify(obj, null, 2))
      }catch(e){}
    }

    console.show = function(){
      alert( this._log.join( "\n" ) )
      this._log = []
    }

    console.warn = console.log
    
    window.onerror = function( msg, url, line ) {
      console.log("ERROR: \"" + msg + "\" at line " + line + " in "+url)
    }
    window.addEventListener( "touchstart", function( e ) {
      if( e.touches.length === 3 ) console.show()
    })
  }

}

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined'){
  module.exports = debug
} else{
  new debug()
  if( !console.error ) console.error = console.log
}
